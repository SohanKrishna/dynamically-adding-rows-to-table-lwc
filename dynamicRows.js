import { LightningElement, track, api, wire } from 'lwc';
import { fireEvent, registerListener, unregisterAllListeners } from "c/pubsub";
import { CurrentPageReference } from "lightning/navigation";
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { dateDifference, calculateYears,formatDate } from 'c/nhUtil';

import { deleteRecord } from 'lightning/uiRecordApi';
import createOrUpdateEmployment from '@salesforce/apex/BRCApplicationDashboardController.createOrUpdateEmployment';
import EMPLOYMENT_OBJECT from '@salesforce/schema/Applicant_Employment_Information__c';
import POSITION_PICKLIST from '@salesforce/schema/Applicant_Employment_Information__c.Role__c';
import { ShowToastEvent } from "lightning/platformShowToastEvent";

export default class DynamicRows extends LightningElement {
    @track pageData = {};
    @track showNewEmployer;
    @track showCurrentEmployer;
    @track currentSelectedEmployment='';
    @track currentSelectedEmploymentToEdit=''; 
    @track positionPicklistValues;
    positionValueToLabelMap = {};
    flag = false;
    @wire(CurrentPageReference) pageRef;

    @track newEmployment = {
        "id": "",
        "programName": "",
        "city": "",
        "startDate1": "",
        "endDate": "",
        "editing": null
    }

    @track newCurrentEmployment = {
        "id":"",
        "Employer":"",
        "EmployerId":"",
        "startDate": "",
        "position": "",
        "positionLabel": "",
        "editing": null
    }

    @track formValidity = {
        "programName": false,
        "city": false,
        "startDate1": false,
        "endDate": false,
        "startDate":false,
        "position":false,
    }

    @api
    get allPageData() {
        return this.pageData;
    }
    set allPageData(value) {
        this.pageData = JSON.parse(JSON.stringify(value));
    }
    get maxDate() {
        let today = new Date();
        let month = today.getMonth() + 1;
        let day = today.getDate();
        day = day.toString().length > 1 ? day : `0${day}`;
        month = month.toString().length > 1 ? month : `0${month}`;
        let maxDate = [today.getFullYear(), month, day].join('-');
        // //console.log(maxDate);
        return maxDate;
        // return [today.getFullYear(), today.getMonth() + 1, today.getDate()].join('-');        
    }

    get minMoveInDate() {
        let startDate = new Date();
        let month = startDate.getMonth() + 1 - 6;
        if(month<0){
            let day = startDate.getDate();
            day = day.toString().length > 1 ? day : `0${day}`;
            month = month * (-1);
            month = 12 - month;
            month = month.toString().length > 1 ? month : `0${month}`;
            let minDate = [startDate.getFullYear()-1, month, day].join('-');
            //console.log('Min Date' + minDate);
            return minDate;
        }
        else{
            let day = startDate.getDate();
            day = day.toString().length > 1 ? day : `0${day}`;
            month = month.toString().length > 1 ? month : `0${month}`;
            let minDate = [startDate.getFullYear(), month, day].join('-');
            //console.log('Min Date' + minDate);
            return minDate;
        }
        // return [today.getFullYear(), today.getMonth() + 1, today.getDate()].join('-');        
    }

    get minDate() {
        let startDate = new Date(this.newEmployment.startDate1);
        let d = new Date();
        d.setMonth(d.getMonth() - 6);
        if (startDate > d) {
            let month = startDate.getMonth() + 1;
            let day = startDate.getDate();
            day = day.toString().length > 1 ? day : `0${day}`;
            month = month.toString().length > 1 ? month : `0${month}`;
            let minDate = [startDate.getFullYear(), month, day].join('-');
            //console.log('Min Date' + minDate);

            return minDate;
        }
        else {
            return this.minMoveInDate;
        }
        // return [today.getFullYear(), today.getMonth() + 1, today.getDate()].join('-');        
    }

    @wire(getObjectInfo, { objectApiName: EMPLOYMENT_OBJECT })
    objectInfo;

    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: POSITION_PICKLIST })
    getPositionPicklistValues({ error, data }) {
        if (error) {
            //console.log('error: ', error);
        } else if (data) {
            this.positionPicklistValues = this.setPositionSelected(data.values,'position');

            console.log('positionValueToLabelMap '+JSON.stringify(this.positionValueToLabelMap));
            this.pageData.householdEmployment.currentEmployment.forEach(emp => {
                emp['positionLabel'] = this.positionValueToLabelMap[emp.position];
            })
            console.log('this.pageData.householdEmployment.currentEmployment '+JSON.stringify(this.pageData.householdEmployment.currentEmployment));
        }
    }

    setPositionSelected(arr,field) {
        let retArray = [];
        console.log(' position arr: ', JSON.stringify(arr));
        /*arr.forEach(element => {    
                if (this.newCurrentEmployment[field] && this.newCurrentEmployment[field] === element.value) {
                    retArray.push({ key: element.value, value: element.value, selected: true });
                } else retArray.push({ key: element.value, value: element.value, selected: false });
        });*/
        arr.forEach(element => {  
            this.positionValueToLabelMap[element.value] = element.label;
            if (this.newCurrentEmployment[field] && this.newCurrentEmployment[field] === element.value) {
                retArray.push({ key: element.value, label: element.label, value: element.value, selected: true });
            } else retArray.push({ key: element.value, label: element.label, value: element.value, selected: false });
        });
        return retArray;
    }

    createNewEmployer() {
        this.showNewEmployer = true;
        this.currentSelectedEmployment='';
        this.newEmployment = {
            "id": "",
            "programName": "",
            "city": "",
            "startDate1": "",
            "endDate": "",
            "editing": null
        }
    }

    closebluebox() {
        this.showNewEmployer = false;
    }

    closeCurrentEmploymentBox(){
        this.showCurrentEmployer=false;
    }

    addToCurrentEmploymentTable() {
        
            // this.newEmployment.start = formatDate(this.newEmployment.startDate1);
            // this.newEmployment.end = formatDate(this.newEmployment.endDate);
            this.pageData.householdEmployment.currentEmployment.splice(this.newCurrentEmployment.editing, 1, { ...this.newCurrentEmployment });
            this.newCurrentEmployment = {
                "id":"",
                "Employer":"",
                "EmployerId":"",
                "startDate": "",
                "position": "",
                "positionLabel": "",
                "editing": null
            }
            this.closeCurrentEmploymentBox();
            this.dispatchData();
    }

    addToTable() {
        this.flag = true;      
        this.template.querySelector("c-custom-lookup").checkValidation();
        this.validateSpecificFields(['startDate1', 'endDate']);
        //let checkDates = dateDifference(this.newEmployment.startDate1, this.newEmployment.endDate);
        const allValidText = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
        if (!allValidText) {
            this.flag=false;
        }
        // if(checkDates) {
        //     this.dispatchEvent(new ShowToastEvent({
        //         title: 'Error',
        //         message: 'Start date should be earlier than End date.',
        //         variant: 'error'
        //     }));
        //     this.flag=false;
        // }
        //console.log(this.currentSelectedEmployment);
        if(this.newEmployment.editing==null && (this.currentSelectedEmployment=='' || this.currentSelectedEmployment==null)){
            this.flag=false;            
        }
        else if(this.newEmployment.editing!=null && (this.currentSelectedEmploymentToEdit=='' || this.currentSelectedEmploymentToEdit==null)){
            this.flag=false;
        }
        //console.log(this.flag);
        if (this.flag) {
            //console.log('New Employer in addtotable ' + JSON.stringify(this.newEmployment));
            this.newEmployment.start = formatDate(this.newEmployment.startDate1);
            this.newEmployment.end = formatDate(this.newEmployment.endDate);
            this.newEmployment.editing ? this.pageData.householdEmployment.previousEmployment.splice(this.newEmployment.editing, 1, { ...this.newEmployment }) : this.pageData.householdEmployment.previousEmployment.splice(0, 0, { ...this.newEmployment });
            this.newEmployment = {
                "id": "",
                "programName": "",
                "city": "",
                "startDate1": "",
                "endDate": "",
                editing: null
            }
            this.currentSelectedEmployment='';
            this.currentSelectedEmploymentToEdit='';
            this.closebluebox();
            this.dispatchData();
        }
    }
    connectedCallback() {
        
        this.pageData.householdEmployment.previousEmployment.forEach( emp => {
            emp.start = (emp.startDate1 ? formatDate(emp.startDate1) : '');
            emp.end = (emp.endDate ? formatDate(emp.endDate) : '');
        });
        //console.log(JSON.stringify(this.pageData.householdEmployment));
    }



    handleChange(event) {

        this.pageData.currentEmploymentList[event.target.dataset.index][event.target.dataset.field] = event.target.value;
        ////console.log('pageData in child: ', JSON.stringify(this.pageData));
        this.validateSpecificFields([`${event.target.dataset.field}`]);
        this.dispatchData();
    }

    handleCurrentEmployerChange(event) {
         this.newCurrentEmployment[event.target.dataset.field] = event.target.value;
         if(event.target.dataset.field == 'position'){
             this.newCurrentEmployment['positionLabel'] = this.positionValueToLabelMap[event.target.value];
         }

     }

    handleNewEmployerChange(event) {
        this.newEmployment[event.target.dataset.field] = event.target.value;
        if (event.target.dataset.field != 'city') {
            this.validateSpecificFields([`${event.target.dataset.field}`]);
        }
    }
    handleAccountSelection(event){
        this.newEmployment.programName = event.detail.Name;
        //console.log(JSON.stringify(event.detail));
        this.newEmployment.city=(event.detail.ShippingCity?event.detail.ShippingCity:'');
        this.currentSelectedEmployment=this.newEmployment.programName;
        if(this.newEmployment.editing!=null){
            this.currentSelectedEmploymentToEdit=this.newEmployment.programName;
        }
        //console.log(this.currentSelectedEmployment)
    }
    handleAccountRemove(event) {
        this.newEmployment.programName = "";
        this.newEmployment.city="";
        this.currentSelectedEmployment='';
        if(this.newEmployment.editing!=null){
            this.currentSelectedEmploymentToEdit='';
        }
    }

    dispatchData() {
        const selectedEvent = new CustomEvent("handlechange", {
            detail: {
                thisPageData: { ...this.pageData }
            }
        });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }

    validateSpecificFields(checks) {
        //console.log('checking fields: ', JSON.stringify(checks));
        //checks.every(func(element){
        checks.forEach(element => {
            //for (var i = 0; i < checks.length; i++) {
            //let element = checks[i];
            let field = this.template.querySelector(`[data-field="${element}"]`);
            //console.log(JSON.stringify(this.newEmployment));
            let str = field.value;
            //console.log(str);
            //console.log(typeof (str));
            if (!field.value) {
                field.classList.add('slds-has-error');
                this.formValidity[element] = true;
                this.flag = false;
                // break;
            }
            else if (str.trim().length === 0) {
                field.value = null;
                field.classList.add('slds-has-error');
                this.formValidity[element] = true;
                //console.log('entered trim');
                this.flag = false;
                //break;
            }
            else {
                field.classList.remove('slds-has-error');
                this.formValidity[element] = false;
                this.flag = true;
            }
        });
    }
    calculateYears(date1) {
        //console.log('date1: ', JSON.stringify(date1));
        let birthDate = new Date(date1);
        let otherDate = new Date();
        let years = (otherDate.getFullYear() - birthDate.getFullYear());
        if (otherDate.getMonth() > birthDate.getMonth() ||
            otherDate.getMonth() === birthDate.getMonth() && otherDate.getDate() > birthDate.getDate()) {
            years--;
        }
        //console.log('Years', years);
        return years;
    }

    @api
    validatePageData(which) {
        //console.log('check validation: ', which);
        let fields = this.template.querySelectorAll('[data-check="req"]');
        let isMissing = false;
        if (fields && fields.length > 0) {
            fields.forEach(element => {
                let str = element.value;
                ////console.log('Length --> ' + str.length);
                if (!element.value) {
                    isMissing = true;
                    // fireEvent(this.pageRef, 'hideSpinner', this);
                    element.classList.add('slds-has-error');
                    this.formValidity[element.dataset.field] = true;
                }
                else if (str.trim().length === 0) {
                    element.value = null;
                    // fireEvent(this.pageRef, 'hideSpinner', this);
                    isMissing = true;
                    element.classList.add('slds-has-error');
                    this.formValidity[element.dataset.field] = true;
                }
                /* else if (element.dataset.field === 'startDate') {
                     let dobVal = this.template.querySelector('[data-field="startDate"]');
                     dobVal = this.calculateYears(dobVal.value);
                     //console.log('dobVal: ', dobVal);
                     if (dobVal < 0) {
                         element.classList.add('slds-has-error');
                         isMissing = true;
                     } else {
                         element.classList.remove('slds-has-error');
                     }
                 }*/
                else {
                    element.classList.remove('slds-has-error');
                    this.formValidity[element.dataset.field] = false;
                }
            });
        }
        if (isMissing) {
            return
        } else {
            fireEvent(this.pageRef, "spinnerTrue", false);
            //console.log(JSON.stringify(this.pageData.householdEmployment));
            createOrUpdateEmployment({
                employmentList: JSON.stringify(this.pageData.householdEmployment),
                appId: this.pageData.applicationId
            })
                .then(prevAdress => {

                    this.pageData.householdEmployment = JSON.parse(prevAdress);
                    this.pageData.householdEmployment.previousEmployment.forEach( emp => {
                        emp.start = (emp.startDate1 ? formatDate(emp.startDate1) : '');
                        emp.end = (emp.endDate ? formatDate(emp.endDate) : '');
                    });
                    //console.log(JSON.stringify(this.pageData.householdEmployment));
                    this.dispatchData();
                    if (which === 'exit') {
                        fireEvent(this.pageRef, "spinnerFalse", false);
                        fireEvent(this.pageRef, "gotodashboard", which);
                    } else {
                        fireEvent(this.pageRef, "validpage", which);
                        fireEvent(this.pageRef, "spinnerFalse", false);
                    }
                })
                .catch(error => {
                    //console.log(JSON.stringify(error));
                    fireEvent(this.pageRef, "spinnerFalse", false);
                });
        }
    }

    editCurrentEmployers(event){
        let index = event.currentTarget.dataset.edit;
        let editing = this.pageData.householdEmployment.currentEmployment[index];
        this.newCurrentEmployment = {
            "id":editing.id,
            "Employer":editing.Employer,
            "EmployerId":editing.EmployerId,
            "startDate": editing.startDate,
            "position": editing.position,
            "positionLabel": editing.positionLabel,
            editing: index
        };     
        this.positionPicklistValues=this.setPositionSelected(this.positionPicklistValues,"position");
        this.showCurrentEmployer = true;
    }

    editPreviousEmployers(event) {

        let index = event.currentTarget.dataset.edit;
        let editing = this.pageData.householdEmployment.previousEmployment[index];
        this.newEmployment = {
            "id": editing.id,
            "programName": editing.programName,
            "city": editing.city,
            "startDate1": editing.startDate1,
            "endDate": editing.endDate,
            editing: index
        };
        //console.log('Edit Index ' + index);
        //console.log(JSON.stringify(this.newEmployment));
        this.currentSelectedEmploymentToEdit=editing.programName;
        this.showNewEmployer = true;
        try{
            setTimeout(() => {
                this.template.querySelector("c-custom-lookup").handleEditInHouseholdEmployement();
            }, 3000);
        }
        catch(error){
            //console.log(error);
        }
        //console.log('Edit Index ' + index);
    }
    removePreviousEmployers(event) {
        let index = event.currentTarget.dataset.remove;
        let deletedPrevEmployement;
        if (this.pageData.householdEmployment.previousEmployment.length > 0) {
            deletedPrevEmployement = this.pageData.householdEmployment.previousEmployment.splice(index, 1);
        }
        //console.log(JSON.stringify(deletedPrevEmployement[0].id));
        this.showNewEmployer = false;
        this.newEmployment = {
            "id": "",
            "programName": "",
            "city": "",
            "startDate1": "",
            "endDate": "",
            "editing": null
        }
        if (deletedPrevEmployement[0].id != "") {
            deleteRecord(deletedPrevEmployement[0].id)
                .then(result => {
                    //console.log('Deleted employment');
                })
                .catch(error => {
                    //console.log('error while deleting adress');
                });
        }
        this.dispatchData();

    }
}